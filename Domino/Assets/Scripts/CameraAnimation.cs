﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraAnimation : MonoBehaviour {

	
    public void CameraAnim()
    {
        this.GetComponent<Animator>().Play("CameraMovement");

    }


    public void LoadApp()
    {
        SceneManager.LoadScene(1);
    }


}
