﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public GameObject rightSpawnTarget;
    public GameObject leftSpawnTarget;

    public GameObject ball;



    public void Zero_one()
    {
        Instantiate(ball, rightSpawnTarget.transform.position,rightSpawnTarget.transform.rotation);
    }

    public void One_zero()
    {
        Instantiate(ball, leftSpawnTarget.transform.position, rightSpawnTarget.transform.rotation);
    }

    public void One_one()
    {
        Instantiate(ball, rightSpawnTarget.transform.position, rightSpawnTarget.transform.rotation);
        Instantiate(ball, leftSpawnTarget.transform.position, rightSpawnTarget.transform.rotation);
    }

	
}
