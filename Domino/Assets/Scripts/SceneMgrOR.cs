﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgrOR : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void NextScene()
    {
        SceneManager.LoadScene(2);
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(1);
    }

    public void PrevScene()
    {
        SceneManager.LoadScene(4);
    }
}
