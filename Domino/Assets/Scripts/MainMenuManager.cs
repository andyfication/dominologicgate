﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour {

    public Canvas MainMenu;

	private void Awake()
	{
        MainMenu.enabled = true;
	}


	public void HideCanvas()
    {
        MainMenu.enabled = false;
    
    }
}
