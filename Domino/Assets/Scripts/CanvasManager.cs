﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour {


    public GameObject buttons;
    public GameObject Res00;
    public GameObject Res01;
    public GameObject Res10;
    public GameObject Res11;

	// Use this for initialization
	void Start () {

        buttons.SetActive(true);
        Res00.SetActive(false);
        Res01.SetActive(false);
        Res10.SetActive(false);
        Res11.SetActive(false);
		
	}
	
	
    public void Zero_zero()
    {
        buttons.SetActive(false);
        Res00.SetActive(true);
        Res01.SetActive(false);
        Res10.SetActive(false);
        Res11.SetActive(false);
    }

    public void Zero_one()
    {
        buttons.SetActive(false);
        Res00.SetActive(false);
        Res01.SetActive(true);
        Res10.SetActive(false);
        Res11.SetActive(false);
    }

    public void One_zero()
    {
        buttons.SetActive(false);
        Res00.SetActive(false);
        Res01.SetActive(false);
        Res10.SetActive(true);
        Res11.SetActive(false);
    }

    public void One_one()
    {
        buttons.SetActive(false);
        Res00.SetActive(false);
        Res01.SetActive(false);
        Res10.SetActive(false);
        Res11.SetActive(true);
    }
}
